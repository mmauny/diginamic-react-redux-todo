import { Button, Input } from "antd";
import { useState } from "react";

export function TodoForm({onNewTodo}) {
  const [title, setTitle] = useState("");
  return (
    <div>
      <Input value={title} onChange={(e) => setTitle(e.target.value)}/>
      <Button onClick={() => onNewTodo({title, isDone:false})}>Ajouter à ma liste</Button>
    </div>
  );
}
