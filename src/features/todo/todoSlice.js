import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  value: [],
};

export const todoSlice = createSlice({
  name: "todo",
  initialState,
  reducers: {
    ajouter: (state, action) => {
      state.value = [...state.value, action.payload];
    },
    chargement: (state, action) => {
      state.value = action.payload;
    },
    supprimerTout: (state) => {
      state.value = [];
    },
    supprimer: (state, action) => {
      state.value = state.value.filter(
        (current) => !action.payload || current.title !== action.payload.title
      );
    },
    done: (state, action) => {
      state.value = state.value.map((current) =>
        current.title === action.payload.title
          ? { ...current, isDone: true }
          : current
      );
    },
    undone: (state, action) => {
      state.value = state.value.map((current) =>
        current.title === action.payload.title
          ? { ...current, isDone: false }
          : current
      );
    },
  },
});

export const {
  ajouter,
  done,
  undone,
  chargement,
  supprimerTout,
  supprimer,
} = todoSlice.actions;
export const selectTodos = (state) => state.todo.value;

export default todoSlice.reducer;
