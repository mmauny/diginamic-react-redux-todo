import todoReducer, { ajouter, done, supprimer } from "./todoSlice";

describe("Test de la fonctionnalité Todo", () => {
  it("Valeur initiale", () => {
    expect(todoReducer(undefined, {})).toEqual({ value: [] });
  });
  it("Ajout dans une liste vide", () => {
    expect(
      todoReducer(
        undefined,
        ajouter({ title: "Faire des courses", isDone: false })
      )
    ).toEqual({ value: [{ title: "Faire des courses", isDone: false }] });
  });
  it("done d'un todo", () => {
    expect(
      todoReducer(
        { value: [{ title: "Faire des courses", isDone: false }] },
        done({ title: "Faire des courses", isDone: false })
      )
    ).toEqual({ value: [{ title: "Faire des courses", isDone: true }] });
  });
  it("done d'un todo qui n'existe pas", () => {
    expect(
      todoReducer(
        { value: [{ title: "Faire des courses", isDone: false }] },
        done({ title: "Apprendre le react", isDone: false })
      )
    ).toEqual({ value: [{ title: "Faire des courses", isDone: false }] });
  });
  it("Suppression d'un todo", () => {
    expect(
      todoReducer(
        { value: [{ title: "Faire des courses", isDone: false }, { title: "Apprendre le react", isDone: false }] },
        supprimer({ title: "Apprendre le react", isDone: false })
      )
    ).toEqual({ value: [{ title: "Faire des courses", isDone: false }] });
  });
  it("Suppression d'un todo vide", () => {
    expect(
      todoReducer(
        { value: [{ title: "Faire des courses", isDone: false }, { title: "Apprendre le react", isDone: false }] },
        supprimer()
      )
    ).toEqual({ value: [{ title: "Faire des courses", isDone: false }, { title: "Apprendre le react", isDone: false }] });
  });
});
