import { Card } from "antd";
import "./Todo.css";
import { DeleteOutlined } from "@ant-design/icons";

export function Todo({ todo, onTodoClick, onTrashClick }) {
  return (
    <Card className={`Todo ${todo.isDone ? "done" : "notDone"}`}>
      <span onClick={() => onTodoClick(todo)}>{todo.title}</span>
      <span onClick={() => onTrashClick(todo)}><DeleteOutlined /></span>
    </Card>
  );
}
