import { Divider } from "antd";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Todo } from "./Todo";
import { TodoForm } from "./TodoForm";
import {
  ajouter,
  chargement,
  done,
  selectTodos,
  undone,
  supprimerTout,
  supprimer,
} from "./todoSlice";
import { DeleteOutlined } from "@ant-design/icons";

export function TodoList() {
  const todos = useSelector(selectTodos);
  const dispatch = useDispatch();

  useEffect(() => {
    const todosFromStorage = localStorage.getItem("todos");
    todosFromStorage && dispatch(chargement(JSON.parse(todosFromStorage)));
  }, [dispatch]);

  useEffect(() => {
    localStorage.setItem("todos", JSON.stringify(todos));
  }, [todos]);

  const handleNewTodo = (todo) => dispatch(ajouter(todo));
  const handleTodoClick = (todo) =>
    dispatch(todo.isDone ? undone(todo) : done(todo));
  const handleDeleteTodo = (todo) => dispatch(supprimer(todo));

  return (
    <div>
      <TodoForm onNewTodo={handleNewTodo}></TodoForm>
      <spans><DeleteOutlined onClick={() => dispatch(supprimerTout())} />Tout supprimer</spans>
      <Divider></Divider>
      {todos.map((todo) => (
        <Todo key={todo.title} todo={todo} onTodoClick={handleTodoClick} onTrashClick={handleDeleteTodo}></Todo>
      ))}
    </div>
  );
}
