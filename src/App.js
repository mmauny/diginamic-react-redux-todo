import React from "react";
import "./App.css";
import { TodoList } from "./features/todo/TodoList";
import "antd/dist/antd.css";
import Layout, { Content, Header } from "antd/lib/layout/layout";
import { Col, Row } from "antd";

function App() {
  return (
    <Layout>
      <Header>TODO</Header>
      <Content>
        <Row>
          <Col span={8}/>
          <Col span={8}>
            <TodoList></TodoList>
          </Col>
          <Col span={8}/>
        </Row>
      </Content>
    </Layout>
  );
}

export default App;
